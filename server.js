const express = require("express");

const app = express();
const {PORT = 3000} = process.env;
const path = require("path");

app.use( express.static(path.join(__dirname, "public")) );
app.get("/", (req, res) => {
    res.sendFile( path.join(__dirname, "public", "index.html") );
});

// Automatically open the web browser after the server is listening:
app.listen(PORT, () => {
    open = require("open");
    open(`http://localhost:${PORT}`);
});