# Komputer store

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

This webpage is the first assignment of the 2022 full stack developer course of Noroff.
The main learning goal of this assignment is to use vanilla javascript to manipulate web pages.
Requirements of the komputer-store webpage can be found in de [pdf in this repo](JavaScript_Komputer%20Store%20App.pdf).

## Installation
You need `npm` to make the website work.
1. Clone or download this repository.
2. Run `npm install` to install all dependencies.
3. Run the server with `npm run start`. The browser with the website will automatically open.

## Usage
- Select a laptop in de dropdown menu.
- Buy the laptop by clicking the 'Buy' button. The laptop can only be bought if
  - there is enough balance on the bank, and
  - there is any in stock.
- Use the 'Get a loan' button to get a loan. Requirements:
  - You can only have one loan at the time.
  - The requested loan must be a positive integer amount. A loan of € 0 will also be rejected.
  - The loan is at most twice the bank balance.
- Increase the pay for work by clicking the 'Work' button.
- Press the 'Bank' button to transfer the pay to the balance. 10% of the transferred pay will be used to repay the loan, if applicable.
- If you have a loan, you can (partially) repay the loan by clicking the 'Repay loan' button. Any excess pay will be transferred to the bank balance.

## License
Copyright &copy; 2022 Kevin Choi

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE. 