// Some constant values:
const API_BASE_URL = "https://noroff-komputer-store-api.herokuapp.com";
const DEFAULT_IMAGE = "https://via.placeholder.com/500?text=No+image+available";

// Get the important elements:
const currentBalanceElement = document.getElementById("current-balance");
const loanDisplayElement = document.getElementById("loan-display");
const currentLoanElement = document.getElementById("current-loan");
const getNewLoanElement = document.getElementById("get-new-loan");
const currentPayElement = document.getElementById("current-pay");
const payToBankElement = document.getElementById("pay-to-bank");
const workElement = document.getElementById("work");
const repayLoanElement = document.getElementById("repay-loan");
const laptopElement = document.getElementById("laptop");
const laptopDescriptionElement = document.getElementById("laptop-description");
const laptopImageElement = document.getElementById("laptop-image");
const laptopSpecificationsElement = document.getElementById("laptop-specifications");
const laptopStockElement = document.getElementById("laptop-stock");
const laptopPriceElement = document.getElementById("laptop-price");
const buyLaptopElement = document.getElementById("buy-laptop");

// Declare and define some state variables:
let currentBalance = parseInt(currentBalanceElement.innerText);
let currentLoan = parseInt(currentLoanElement.innerText);
let currentPay = parseInt(currentPayElement.innerText);
let laptops;
let selectedLaptopIndex; // Note: this is an array index, not the id in the laptops json.

// Fetch laptops from external source:
(async () => {
    try {
        const response = await fetch(`${API_BASE_URL}/computers`);
        const json = await response.json();
        initializeKomputerStore(json);
    } catch (error) {
        alert(`An error occurred while loading the available laptops: ${error.message}`);
    }
})();

// Functions:

// Initialize the website once the json with laptop data has successfully been loaded:
function initializeKomputerStore(json) {
    laptops = json;
    for (let i=0; i<laptops.length; i++) {
        laptopElement.insertAdjacentHTML("beforeend", `<option value=${i}>${laptops[i].title}</option>`);
    }
    if (laptops.length > 0) loadSelectedLaptop(parseInt(laptopElement.value));
}

// Load the details of the selected laptop:
function loadSelectedLaptop(index) {
    selectedLaptopIndex = index;
    const laptop = laptops[index];
    laptopDescriptionElement.innerText = laptop.description;
    laptopStockElement.innerText = laptop.stock;
    laptopPriceElement.innerText = laptop.price;
    laptopImageElement.src = `${API_BASE_URL}/${laptop.image}`;

    // The specifications list:
    laptopSpecificationsElement.innerHTML = "";
    for (let spec of laptop.specs) {
        laptopSpecificationsElement.insertAdjacentHTML("beforeend", `<li>${spec}</li>`);
    }
}

// Function that takes care of properly changing the loan to the given input value.
function changeLoan(newLoanValue) {
    currentLoanElement.innerText = currentLoan = newLoanValue;
    if (newLoanValue === 0) {
        loanDisplayElement.classList.add("d-none");
        repayLoanElement.classList.add("d-none")
    } else {
        loanDisplayElement.classList.remove("d-none");
        repayLoanElement.classList.remove("d-none");
    }
}

// Take care of properly changing the balance to the given input value.
function changeBalance(newBalanceValue) {
    currentBalanceElement.innerText = currentBalance = newBalanceValue;
}

// Take care of properly changing the pay to the given input value.
function changePay(newPayValue) {
    currentPayElement.innerText = currentPay = newPayValue;
}

// Take care of a new loan:
function getNewLoan() {
    try {
        // New loans are not allowed if the last one is not fully repaid.
        if (currentLoan > 0) {
            throw new Error("cannot get a new loan before the last loan is fully repaid");
        }
        // Input loan, check if input is valid.
        const requestedLoan = parseInt(prompt("Enter a positive integer (amount in €):"));
        if (isNaN(requestedLoan) || requestedLoan <= 0) {
            throw new Error("input is not a positive integer");
        }
        // New loans may not be more than double than your current balance.
        if (requestedLoan > 2*currentBalance) {
            throw new Error("new loans may not be more than double than your current balance");
        }
        changeLoan(requestedLoan);
        changeBalance(requestedLoan + currentBalance);

    } catch (error) {
        alert(`Request for new loan rejected: ${error.message}.`);
    }
}

// Put the pay on the bank, taking into account the loan.
// Up to 10% of the pay should be used to repay the loan.
function transferPayToBank() {
    const forLoan = Math.min(currentLoan, currentPay/10);
    changeLoan(currentLoan - forLoan);
    changeBalance(currentBalance + currentPay - forLoan);
    changePay(0);
}

// Use the pay to repay the loan.
// Any excess pay will be transferred to the bank balance.
function repayLoan() {
    const excessPay = Math.max(0, currentPay - currentLoan);
    const leftoverLoan = Math.max(0, currentLoan - currentPay);
    changeLoan(leftoverLoan);
    changeBalance(currentBalance + excessPay);
    changePay(0);
}

// Buy the currently selected laptop.
function buyLaptop() {
    const laptop = laptops[selectedLaptopIndex];
    try {
        // A laptop can only be bought if there is any in stock.
        if (laptop.stock <= 0) {
            throw new Error(`out of stock`);
        }
        // Futhermore, you need sufficient bank balance.
        if (laptop.price > currentBalance) {
            throw new Error(`insufficient bank balance`)
        }

        changeBalance(currentBalance - laptop.price);
        laptopStockElement.innerText = --laptop.stock;
        alert(`Your purchase of ${laptop.title} has been successful!`);
    } catch (error) {
        alert(`Purchase of ${laptop.title} failed: ${error.message}.`);
    }
}

// Set event handlers:
getNewLoanElement.addEventListener("click", getNewLoan);
payToBankElement.addEventListener("click", transferPayToBank);
workElement.addEventListener("click", () => changePay(currentPay + 100));
repayLoanElement.addEventListener("click", repayLoan);
laptopElement.addEventListener("change", () => loadSelectedLaptop(parseInt(laptopElement.value)));
buyLaptopElement.addEventListener("click", buyLaptop);
laptopImageElement.addEventListener("error", () => laptopImageElement.src = DEFAULT_IMAGE); // In case the image fails to load.